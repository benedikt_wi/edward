#!/usr/bin/python3

import edward_config
import sqlite3
import datetime
import gpg

try:
    con = sqlite3.connect('file:' + edward_config.pubkey_database + '?mode=rw', uri=True)
    #os.stat(edward_config.pubkey_database)
except sqlite3.OperationalError:
    con = sqlite3.connect(edward_config.pubkey_database)
    con.execute("CREATE TABLE imports (fingerprint text PRIMARY KEY, date int)")
    con.commit()
    sys.exit() # Notrhing to do

gpgme_ctx = gpg.Context(home_dir=edward_config.gnupghome)

cur = con.cursor()
timelimit = int(datetime.datetime.now().timestamp()) - edward_config.pubkey_retentention_time;

for fingerprint in cur.execute("select fingerprint from imports where date < (?);", (timelimit,)).fetchall():
    key = gpgme_ctx.get_key(fingerprint[0])
    gpgme_ctx.op_delete(key, False)


cur.execute("delete from imports where date < (?);", (timelimit,))

con.commit()
con.close()
