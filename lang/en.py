#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""*********************************************************************
* This file is part of Edward the GPG Bot.                             *
*                                                                      *
* Edward is free software: you can redistribute it and/or modify       *
* it under the terms of the GNU Affero Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or    *
* (at your option) any later version.                                  *
*                                                                      *
* Edward is distributed in the hope that it will be useful,            *
* but WITHOUT ANY WARRANTY; without even the implied warranty of       *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
* GNU Affero Public License for more details.                          *
*                                                                      *
* You should have received a copy of the GNU Affero Public License     *
* along with Edward.  If not, see <http://www.gnu.org/licenses/>.      *
*                                                                      *
* Josh Drake, Copyright (c) 2014                                       *
* Lisa Marie Maginnis, Copyright (c) 2014                              *
* Benedikt Wildenhain, Copyright (c) 2022                              *
*********************************************************************"""

replies = {
    'greeting' : "Hello, I am Edward, the friendly GnuPG bot.",
    'success_decrypt' : "I received your message and decrypted it.",
    'quote_follows' : "Here's a copy of your message:",
    'public_key_received' : 'I received your public key. Thanks. I will remove it from my database again after at most 33 days.',
    'failed_decrypt' : "I'm sorry, I was not able to decrypt your message. Are you sure you encrypted it with my public key?",
    'no_public_key' : "I'm sorry, I was not able to find your public key. Did you remember to attach it?",
    'sig_failure' : 'Your signature could not be verified.',
    'sig_success' : 'Your signature was verified.',
    'my_public_key' : 'If your software cannot find my public key automatically you can get it at the following address: https://keys.openpgp.org/search?q=',
    'signature' : '- Edward, the friendly GnuPG bot\nThe Free Software Foundation created me.\n\nCan you donate to support their work?\nhttps://www.fsf.org/donate',
    'source_code_address' : 'My source code can be downloaded at the following address: ',
    'space' : " "
}


