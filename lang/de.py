#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""*********************************************************************
* This file is part of Edward the GPG Bot.                             *
*                                                                      *
* Edward is free software: you can redistribute it and/or modify       *
* it under the terms of the GNU Affero Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or    *
* (at your option) any later version.                                  *
*                                                                      *
* Edward is distributed in the hope that it will be useful,            *
* but WITHOUT ANY WARRANTY; without even the implied warranty of       *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
* GNU Affero Public License for more details.                          *
*                                                                      *
* You should have received a copy of the GNU Affero Public License     *
* along with Edward.  If not, see <http://www.gnu.org/licenses/>.      *
*                                                                      *
* Josh Drake, Copyright (c) 2014                                       *
* Lisa Marie Maginnis, Copyright (c) 2014                              *
* Benedikt Wildenhain, Copyright (c) 2022                              *
*********************************************************************"""

replies = {
    'greeting' : "Hallo, Ich bin Edward, der freundliche GnuPG Roboter.",
    'success_decrypt' : "Ich habe deine E-Mail empfangen und entschlüsselt.",
    'quote_follows' : "Hier ist eine Kopie deiner Nachricht:",
    'public_key_received' : 'Ich habe deinen öffentlichen Schlüssel erhalten. Danke. Ich werde Ihn nach maximal 33 Tagen wieder aus meiner Datenbank löschen.',
    'failed_decrypt' : "Tut mir leid, ich konnte deine Nachricht nicht entschlüsseln. Bist du dir sicher, dass du sie mit meinem öffentlichen Schlüssel verschlüsselt hast?",
    'no_public_key' : "Tut mir leid, ich konnte deinen öffentlichen Schlüssel nicht finden. Hast du daran gedacht, ihn an die E-Mail anzuhängen?",
    'sig_failure' : 'Deine Signatur konnte nicht verifiziert werden.',
    'sig_success' : 'Deine Signatur wurde erfolgreich verifiziert.',
    'my_public_key' : 'Falls deine Software meinen Schlüssel nicht automatisch findet, kannst du ihn unter der folgenden Adresse herunterladen: https://keys.openpgp.org/search?q=',
    'source_code_address' : 'Du findest meinen Quelltext unter der folgenden Adresse: ',
    'signature' : '- Edward, der freundliche GnuPG Roboter\nDie Free Software Foundation hat mich erstellt. Kannst du etwas spenden, um ihre Arbeit zu unterstützen? | https://www.fsf.org/donate',
    'space' : " "
}


